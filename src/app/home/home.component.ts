import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Doctor } from '../model/doctor';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  doctorsList: Array<Doctor>;

  constructor(
    private apiService: ApiService
  ) {}

  ngOnInit() {
    this.apiService.getDoctorsList().subscribe((data: Array<Doctor>) => {
      this.doctorsList = data;
    }, () => {
      console.log('Anonymous error!');
    });

  }
}
