import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { ApiService } from '../services/api.service';
import { DoctorDetails } from '../model/doctor-details';

@Component({
  selector: 'app-doctor-details',
  templateUrl: './doctor-details.component.html',
  styleUrls: ['./doctor-details.component.scss']
})
export class DoctorDetailsComponent implements OnInit {

  doctorsDetails: DoctorDetails;
  isStar: boolean;

  constructor(
    private router: ActivatedRoute,
    private service: ApiService,
  ) { }

  ngOnInit() {

    const doctorsName = this.router.snapshot.paramMap.get('name');
    this.router.data.subscribe((data: Data) => {

      const doctorsFound = data['details'].filter(x => x.name === doctorsName);
      if (doctorsFound.length > 0) {
        this.doctorsDetails = doctorsFound[0];

        this.isStar = (this.doctorsDetails.rating.filter(x => x.ratings === 5).length) > (this.doctorsDetails.rating.length / 2);
        console.log(this.isStar);
      }

    });

  }

}
