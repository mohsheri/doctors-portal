import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DoctorDetailsComponent } from './doctor-details/doctor-details.component';
import { DoctorDetailsResolver } from './services/doctor-details.resolver';

const routes: Routes = [{
  path: 'home',
  component: HomeComponent
},
{
  path: 'details/:name',
  component: DoctorDetailsComponent,
  resolve: { details: DoctorDetailsResolver }
},
{
  path: '',
  redirectTo: 'home',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
