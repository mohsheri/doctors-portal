export interface PatientRating {
  doctorId: number;
  comments: string;
  ratings: number;
}
