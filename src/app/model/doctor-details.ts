import { PatientRating } from "./patient-rating";

export interface DoctorDetails {
  name: string;
  gender: string;
  specialties: string;
  languageSpoken: string;
  medicalSchool: string;
  patientReview: string;
  rating: Array<PatientRating>;
}
