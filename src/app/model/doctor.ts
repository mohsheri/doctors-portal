export interface Doctor {
    id: number;
    name: string;
    gender: string;
    medicalSchoolId: string;
    languageId: string;
}
