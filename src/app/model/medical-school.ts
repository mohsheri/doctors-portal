export interface MedicalSchool {
  id: number;
  name: string;
}
