export interface DoctorSpecialty {
  doctorId: number;
  specialtyId: number;
}
