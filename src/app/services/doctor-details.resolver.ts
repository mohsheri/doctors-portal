
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { DoctorDetails } from '../model/doctor-details';
import { ApiService } from './api.service';

@Injectable({ providedIn: 'root' })
export class DoctorDetailsResolver implements Resolve<Array<DoctorDetails>> {

  constructor(private service: ApiService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Array<DoctorDetails>> {
    const doctorsName = route.paramMap.get('name');

    return this.service.getDoctorsDetails(doctorsName);
  }
}
