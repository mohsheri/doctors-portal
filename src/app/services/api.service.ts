import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Doctor } from '../model/doctor';
import { DoctorDetails } from '../model/doctor-details';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseJsonURL = 'assets';

  constructor(private http: HttpClient) {  }

  public getDoctorsList(): Observable<Array<Doctor>> {
    return this.http.get<Array<Doctor>>(`${this.baseJsonURL}/doctors_data.json`);
  }

  public getDoctorsDetails(doctorsName: string): Observable<Array<DoctorDetails>> {
    return this.http.get<Array<DoctorDetails>>(`${this.baseJsonURL}/doctors_details.json`);
  }
}
